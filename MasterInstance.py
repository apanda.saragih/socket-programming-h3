import socket
import errno
from socket import error as socket_error

HEADER = 64
PORT = 8889     # Port di inbound rules
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "DISCONNECT"
SERVER = socket.gethostbyname(socket.gethostname()) # Public IPv4 address EC2
ADDR = (SERVER, PORT)
isConnected = False

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  

def send(msg):
    message = msg.encode(FORMAT)
    message_length = len(message)
    send_length = str(message_length).encode(FORMAT)
    send_length += b' ' * (HEADER - len(send_length))
    client.send(send_length)
    client.send(message)
    print(client.recv(2048).decode(FORMAT))

def connect_to_server():
    global isConnected
    global SERVER
    global ADDR
    global client
    try:
        client.connect(ADDR)
        isConnected = True
        print("worker status : active")
        print("Trying to connect to worker....")
        send("Succesfully connected to worker!\n")
        print("\nPlease add time flags (slow, average, fast) at the end of your input." + 
            " No flag means fast working job. \n" + 
            "e.g: input [slow]")
        print("\nInput 'exit' to end the connection. \n" + 
            "If you're disconnected, input 'connect' to reconnect.")
        while isConnected:
            print("Type your input: ")
            inp = input()
            flag = inp.split().pop()
            delay = ''
            if flag == "[slow]":
                delay = '8s'
            elif flag == "[average]":
                delay = '5s'
            elif flag == "[fast]":
                delay = '1s'
            else:
                delay = '1s'
            
            if(isConnected):
                print(f"send messages....waiting about '{delay}'\n")
                send(inp)
                
                if "exit" == inp.strip().lower():
                    print("Disconecting...\n")
                    send(DISCONNECT_MESSAGE)
                    isConnected = False
            else:
                print("You currently not connected to any worker")
                print("Type 'CONNECT' to connect to the worker\n")
                if "connect" == inp.strip().lower():
                    isConnected = True 
    except:
        print("worker status: dead")

connect_to_server()


