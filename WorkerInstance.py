import socket
import threading
import time

HEADER = 64
PORT = 8889 # Port di inbound rules

SERVER = socket.gethostbyname(socket.gethostname()) # Private IPv4 addresses EC2
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT'

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)

def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")
    connected = True
    while connected:
        message_length = conn.recv(HEADER).decode(FORMAT)
        if message_length:
            message_length = int(message_length)
            message = conn.recv(message_length).decode(FORMAT)
            messageSplit = message.split() 
            if len(message) == 0:
                conn.send(f"Please input something!".encode(FORMAT))
            else:
                if message == DISCONNECT_MESSAGE:
                    print("Bye!")
                    connected = False
                
                output = ""
                for string in messageSplit:
                    if string == "[slow]":
                        pass
                    elif string == "[average]":
                        pass
                    elif string == "[fast]":
                        pass  
                    else:
                        output += string + " "

                if output == "":
                    print(f"[{addr}] {message}")
                else:
                    print(f"[{addr}] {output}")
                
                res = ""
                if len(message)%2 == 0:
                    res = "EVEN"
                else:
                    res = "ODD"
                
                if messageSplit[-1] == "[slow]":
                    time.sleep(8)
                elif messageSplit[-1] == "[average]":
                    time.sleep(5)
                elif messageSplit[-1] == "[fast]":
                    time.sleep(1)
                else:
                    time.sleep(1)
                conn.send(f"Message received: '{message}' with {res} length!".encode(FORMAT))
    
    conn.close()

def start():
    server.listen()
    print(f"[LISTENING] server is listening to {SERVER}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")

print("[STARTING] server is starting...")
start()
print("Byeee!")

