**Socket Programming**


Meroketnya trend Data Science dan Big Data membuat infrastruktur Cloud Computing semakin sibuk dan banyak digunakan. Salah satu teknologi yang menopang kinerja Cloud adalah job management system yang mengatur penjadwalan jobs yang di submit untuk dieksekusi di lingkungan Cloud. Beberapa teknologi yang cukup populer diantaranya adalah (silahkan dipelajari mandiri).

Project yang kami buat mengimplementasikan paradigma Master-Worker, dimana Master bertugas memberikan job kepada worker(s) yang ada.
Dalam project ini, komputer kami bertugas sebagai Master, dan AWS EC2 bertugas sebagai worker. Kami menggunakan bahasa pemrograman Python dengan library socket untuk bisa menghubungkan master dengan worker.

Hal yang kami implementasikan di project ini adalah Master yang mengirim sebuah String dengan flag tertentu yang nantinya diterima oleh Worker dan dihitung panjang stringnya (apakah ganjil atau genap) selama waktu tertentu sesuai flag yang Master berikan ([slow] = 8 detik, [average] = 5 detik, dan [fast] = 1 detik).

Contoh:

Kita sebagai client menginput String “hari ini cerah [slow]” dengan flag [slow], maka worker akan mengembalikan String tersebut ke Master dengan waktu jeda yang agak lama (8 detik). Kalau flagnya diganti dengan [fast], maka Worker akan mengembalikan String tersebut ke Master dengan jeda waktu singkat (1 detik).

Jarkom-C, Kelompoh H3:
Apandaoswalen	- 1806141145
Fadhil Ibrahim	- 1806186692
Habel C.T	    - 1806186761
